import tkinter as tk
from tkinter import scrolledtext

class MIC1Simulator:
    def __init__(self, master):
        self.master = master
        self.master.title("MIC-1 Simulator")
        
        self.code = []
        self.memory = [0] * 4096
        self.data_cache = [None] * 10
        self.instruction_cache = [None] * 10

        #Inicializando os registradores
        self.registers = {
            'PC': 0, 'AC': 0, 'SP': 4096, 'IR': '0' * 16, 'TIR': 0,
            '0': 0, '+1': 1, '-1': -1, 'AMASK': 4095, 'SMASK': 255,
            'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0,
            'MPC': 0, 'MIR': 0, 'MAR': 0, 'MBR': 0
        }
        
        self.create_widgets()
        self.update_ui()
        self.master.geometry("") 

    #Criação da interface gráfica no tkinter
    def create_widgets(self):
        left_frame = tk.Frame(self.master)
        left_frame.pack(side=tk.LEFT, fill=tk.Y, padx=10, pady=10)

        right_frame = tk.Frame(self.master)
        right_frame.pack(side=tk.RIGHT, fill=tk.Y, padx=10, pady=10)

        self.code_label = tk.Label(left_frame, text="Código (em binário)")
        self.code_label.pack()
        self.code_text = scrolledtext.ScrolledText(left_frame, height=10, width=50)
        self.code_text.pack()

        button_frame = tk.Frame(left_frame)
        button_frame.pack(pady=10)

        self.load_button = tk.Button(button_frame, text="Carregar Código", command=self.load_program)
        self.load_button.pack(side=tk.LEFT, padx=5)

        self.run_button = tk.Button(button_frame, text="Próximo Passo", command=self.step)
        self.run_button.pack(side=tk.LEFT, padx=5)

        self.reset_button = tk.Button(button_frame, text="Resetar Simulação", command=self.reset_simulation)
        self.reset_button.pack(side=tk.LEFT, padx=5)

        self.current_instruction_label = tk.Label(left_frame, text="Instrução Decodificada:")
        self.current_instruction_label.pack(pady=(10, 0))
        self.current_instruction_value = tk.Label(left_frame, text="")
        self.current_instruction_value.pack()

        self.memory_label = tk.Label(left_frame, text="Memória")
        self.memory_label.pack(pady=(20, 0))
        self.memory_text = scrolledtext.ScrolledText(left_frame, height=10, width=50)
        self.memory_text.pack()

        self.data_cache_label = tk.Label(left_frame, text="Cache de Dados")
        self.data_cache_label.pack(pady=(10, 0))
        self.data_cache_text = scrolledtext.ScrolledText(left_frame, height=10, width=50)
        self.data_cache_text.pack()

        self.instruction_cache_label = tk.Label(right_frame, text="Cache de Instruções")
        self.instruction_cache_label.pack(pady=(10, 0))
        self.instruction_cache_text = scrolledtext.ScrolledText(right_frame, height=10, width=50)
        self.instruction_cache_text.pack()

        self.register_labels = {}
        self.register_values = {}

        #Aparece os registradores e os valores
        for reg in self.registers:
            frame = tk.Frame(right_frame)
            frame.pack(anchor='w')
            self.register_labels[reg] = tk.Label(frame, text=f"{reg}:")
            self.register_labels[reg].pack(side=tk.LEFT)
            self.register_values[reg] = tk.Label(frame, text="")
            self.register_values[reg].pack(side=tk.LEFT)

        #Mensagem de Fim do programa
        self.end_program_label = tk.Label(right_frame, text="", fg="red")
        self.end_program_label.pack(pady=(20, 0))

    #Função de resetar simulador
    def reset_simulation(self):
        self.code = []
        self.memory = [0] * 4096
        self.data_cache = [None] * 10
        self.instruction_cache = [None] * 10

        #Resetando registradores
        self.registers = {
            'PC': 0, 'AC': 0, 'SP': 4096, 'IR': '0' * 16, 'TIR': 0,
            '0': 0, '+1': 1, '-1': -1, 'AMASK': 4095, 'SMASK': 255,
            'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0,
            'MPC': 0, 'MIR': 0, 'MAR': 0, 'MBR': 0
        }

        #Limpar o conteúdo da caixa de texto
        self.code_text.delete("1.0", tk.END)
        self.update_ui()
        self.end_program_label.config(text="")  

    #Função que acrrega o código digitado na interface
    def load_program(self):
        code = self.code_text.get("1.0", tk.END).strip().split('\n')
        self.code = [line.strip() for line in code if line.strip()]
        self.registers['PC'] = 0
        self.update_ui()

    def update_ui(self):
        #Atualiza a interface com os valores dos registradores e memória
        for reg in self.registers:
            if reg == 'IR':
                self.register_values[reg].config(text=f"{self.registers[reg]}")
            else:
                self.register_values[reg].config(text=f"{self.registers[reg]}")

        self.memory_text.delete(1.0, tk.END)
        for i in range(len(self.memory)):
            self.memory_text.insert(tk.END, f"Endereço {i}: {self.memory[i]:016b} ({self.memory[i]})\n")

        self.data_cache_text.delete(1.0, tk.END)
        for i, v in enumerate(self.data_cache):
            if v is not None:
                self.data_cache_text.insert(tk.END, f"Endereço {i}: {v:016b}\n")
            else:
                self.data_cache_text.insert(tk.END, f"Endereço {i}: {'----' * 4}\n")

        self.instruction_cache_text.delete(1.0, tk.END)
        for i, v in enumerate(self.instruction_cache):
            if v is not None:
                self.instruction_cache_text.insert(tk.END, f"Endereço {i}: {v}\n")
            else:
                self.instruction_cache_text.insert(tk.END, f"Endereço {i}: {'----' * 4}\n")

    #Busca a instrução da memória usando o PC
    def fetch_instruction(self):
        instruction = self.code[self.registers['PC']]
        self.registers['IR'] = instruction
        self.instruction_cache[self.registers['PC'] % 8] = instruction
        return instruction

    #Função que decodifica a instrução usando o opcode
    def decode_instruction(self, instruction):
        opcode = instruction[:4]
        address = int(instruction[4:], 2)
        opcode_map = {
            '0000': 'LODD',
            '0001': 'STOD',
            '0010': 'ADDD',
            '0011': 'SUBD',
            '0100': 'JPOS',
            '0101': 'JZER',
            '0110': 'JUMP',
            '0111': 'LOCO',
            '1000': 'LODL',
            '1009': 'STOL',
            '1010': 'ADDL',
            '1011': 'SUBL',
            '1100': 'JNEG',
            '1101': 'JNZE',
            '1110': 'CALL',
            '1111': {
                '0000000000000000': 'PSHI',
                '0010000000000000': 'POPI',
                '0100000000000000': 'PUSH',
                '0110000000000000': 'POP',
                '1000000000000000': 'RETN',
                '1010000000000000': 'SWAP',
                '1100': 'INSP',  
                '1110': 'DESP'
            }
        }
        if opcode == '1111':
            sub_opcode = instruction[4:]
            decoded = opcode_map[opcode].get(sub_opcode, '????')
            if decoded in ['INSP', 'DESP']:
                return f"{decoded} {int(sub_opcode[4:], 2)}"
            else:
                return decoded
        else:
            return opcode_map.get(opcode, '????') + f" {address}"

    def execute_instruction(self, instruction):
        opcode = instruction[:4]
        address = int(instruction[4:], 2)

        #Opcode do LODD
        if opcode == '0000':  
            self.registers['MAR'] = address
            self.registers['MBR'] = self.memory[address]
            self.registers['AC'] = self.registers['MBR']
            self.data_cache[address % 8] = self.memory[address]

        #Opcode do STODD
        elif opcode == '0001':  
            self.registers['MAR'] = address
            self.registers['MBR'] = self.registers['AC']
            self.memory[address] = self.registers['MBR']
            self.data_cache[address % 8] = self.memory[address]

        #Opcode do ADDD
        elif opcode == '0010':  
            self.registers['MAR'] = address
            self.registers['MBR'] = self.memory[address]
            self.registers['AC'] += self.registers['MBR']
            self.data_cache[address % 8] = self.memory[address]

        #Opcode do SUBD
        elif opcode == '0011':  
            self.registers['MAR'] = address
            self.registers['MBR'] = self.memory[address]
            self.registers['AC'] -= self.registers['MBR']
            self.data_cache[address % 8] = self.memory[address]

        #Opcode do JPOS
        elif opcode == '0100':  
            if self.registers['AC'] > 0:
                self.registers['PC'] = address

        #Opcode do JZER
        elif opcode == '0101':  
            if self.registers['AC'] == 0:
                self.registers['PC'] = address

        #Opcode do JUMP
        elif opcode == '0110':  
            self.registers['PC'] = address

        #Opcode do LOCO    
        elif opcode == '0111':  
            self.registers['AC'] = address

        #Opcode do LODL
        elif opcode == '1000':  
            self.registers['AC'] = self.memory[self.registers['SP'] + address]

        #Opcode do STOL
        elif opcode == '1001':  
            self.memory[self.registers['SP'] + address] = self.registers['AC']
        
        #Opcode do ADDL
        elif opcode == '1010':  
            self.registers['AC'] += self.memory[self.registers['SP'] + address]

        #Opcode do SUBL
        elif opcode == '1011':  
            self.registers['AC'] -= self.memory[self.registers['SP'] + address]

        #Opcode do JNEG
        elif opcode == '1100':  
            if self.registers['AC'] < 0:
                self.registers['PC'] = address
        
        #Opcode do JNZE
        elif opcode == '1101':  
            if self.registers['AC'] != 0:
                self.registers['PC'] = address

        #Opcode do CALL
        elif opcode == '1110': 
            self.memory[self.registers['SP']] = self.registers['PC']
            self.registers['SP'] -= 1
            self.registers['PC'] = address
        elif opcode == '1111':
            sub_opcode = instruction[4:]
            if sub_opcode == '0000000000000000':  #PSHI
                self.memory[self.registers['SP']] = self.registers['AC']
                self.registers['SP'] -= 1
            elif sub_opcode == '0010000000000000':  #POPI
                self.registers['SP'] += 1
                self.registers['AC'] = self.memory[self.registers['SP']]
            elif sub_opcode == '0100000000000000':  #PUSH
                self.memory[self.registers['SP']] = self.registers['PC']
                self.registers['SP'] -= 1
            elif sub_opcode == '0110000000000000':  #POP
                self.registers['SP'] += 1
                self.registers['PC'] = self.memory[self.registers['SP']]
            elif sub_opcode == '1000000000000000':  #RETN
                self.registers['SP'] += 1
                self.registers['PC'] = self.memory[self.registers['SP']]
            elif sub_opcode == '1010000000000000':  #SWAP
                self.registers['AC'], self.registers['PC'] = self.registers['PC'], self.registers['AC']
            elif sub_opcode.startswith('1100'):  #INSP
                value = int(sub_opcode[4:], 2)
                self.registers['SP'] += value
            elif sub_opcode.startswith('1110'):  #DESP
                value = int(sub_opcode[4:], 2)
                self.registers['SP'] -= value

        decoded_instruction = self.decode_instruction(instruction)
        self.current_instruction_value.config(text=decoded_instruction)
        self.update_ui()

    def step(self):
        if self.registers['PC'] < len(self.code):
            instruction = self.fetch_instruction()
            self.execute_instruction(instruction)
            if self.registers['PC'] < len(self.code):
                self.registers['PC'] += 1
        else:
            self.end_program_label.config(text="FIM DO PROGRAMA")

if __name__ == "__main__":
    root = tk.Tk()
    app = MIC1Simulator(root)
    root.mainloop()